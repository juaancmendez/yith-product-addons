<?php
/**
 * Plugin Name: YITH Product Addons for WooCommerce
 * Description: Product Addons for WooCommerce for YITH Plugins
 * Version: 1.1.0
 * Author: Juan Coronel Mendez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-product-addons
 *
 * @package .
 */

! defined( 'ABSPATH' ) && exit;   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PAWC_VERSION' ) ) {
	define( 'YITH_PAWC_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PAWC_DIR_URL' ) ) {
	define( 'YITH_PAWC_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PAWC_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PAWC_DIR_ASSETS_URL', YITH_PAWC_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PAWC_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PAWC_DIR_ASSETS_CSS_URL', YITH_PAWC_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PAWC_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PAWC_DIR_ASSETS_JS_URL', YITH_PAWC_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PAWC_DIR_PATH' ) ) {
	define( 'YITH_PAWC_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PAWC_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PAWC_DIR_INCLUDES_PATH', YITH_PAWC_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PAWC_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PAWC_DIR_TEMPLATES_PATH', YITH_PAWC_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PAWC_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PAWC_DIR_VIEWS_PATH', YITH_PAWC_DIR_PATH . 'views' );
}

// Different way to declare a constant.

! defined( 'YITH_PAWC_INIT' ) && define( 'YITH_PAWC_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PAWC_SLUG' ) && define( 'YITH_PAWC_SLUG', 'yith-product-addons' );
! defined( 'YITH_PAWC_SECRETKEY' ) && define( 'YITH_PAWC_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );
! defined( 'YITH_PAWC_OPTIONS_PATH' ) && define( 'YITH_PAWC_OPTIONS_PATH', YITH_PAWC_DIR_PATH . 'plugin-options' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_pawc_init_classes' ) ) {
	/**
	 * Yith_pawc_init_classes
	 *
	 * @return void
	 */
	function yith_pawc_init_classes() {

		load_plugin_textdomain( 'yith-product-addons', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PAWC_DIR_INCLUDES_PATH . '/class-yith-pawc-product-addons.php';

		if ( class_exists( 'YITH_PAWC_Product_Addons' ) ) {
			/*
			*	Call the main function
			*/
			yith_pawc_product_addons();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pawc_init_classes', 11 );
