jQuery( document ).ready( function( $ ) {
   
    var index = 0;

    // Clone of the template for the new addon.
    addNewAddon = function(){
        var totalContainer  =  $( this ).closest( '.yith-pawc-total-container' ),
            addonsContainer = totalContainer.find( '.yith-pawc-addons' ),          
            addonsTemplate  = totalContainer.find( '.yith-pawc-template' ),
            newAddon        = $( addonsTemplate.html().replaceAll( '{{INDEX}}', index ) ),
            newAddonIndex   = newAddon.find( '.yith-pawc-addon-index' );
        
        newAddonIndex.attr( 'value', index );
        addonsContainer.append( newAddon );
        initAddon( newAddon );
        //newAddon.removeClass( 'yith-pawc-addon-container__closed' );
        newAddon.find( '.yith-pawc-addons' ).show();
        scrollTo( newAddon );
        index ++;
    };

    // Default options of the new addon.
    initAddon = function( addOn ) {        
        addOn.find( '.yith-pawc__radio' ).trigger( 'change' );
        addOn.find( '.yith-pawc-field_type' ).trigger( 'change' );
        addOn.find( '.yith-pawc-name' ).trigger( 'keyup' );
        addOn.find( '.yith-pawc-addon' ).hide();
        jQuery( '.yith-pawc-option-container:first-child .yith-pawc-delete-option-button' ).hide();
        //addOn.addClass( 'yith-pawc-addon-container__closed' );
    };

    // Show the body of the new addon throw the template.
    arrowAddon = function() {
        var addonContainer = $( this ).closest( '.yith-pawc-addon-container' );
            addon          = addonContainer.find( '.yith-pawc-addon' ),
            arrowContainer = addonContainer.find( '.yith-pawc-header__arrow' ),            
            arrowClassName = addonContainer.attr( 'class' );
        addon.slideToggle();
        addonContainer.toggleClass( 'yith-pawc-total-container__closed' );   
        if ( arrowClassName === 'yith-pawc-addon-container yith-pawc-total-container__closed' ) {            
            arrowContainer.removeClass( 'dashicons-arrow-down-alt2' );
            arrowContainer.addClass( 'dashicons-arrow-up-alt2' )
        } else {            
            arrowContainer.removeClass( 'dashicons-arrow-up-alt2' );
            arrowContainer.addClass( 'dashicons-arrow-down-alt2' )
        }
    };   
    
    // Delete html of the addon when click in your 'remove addon' button.
    removeAddon = function() {
        var addonContainer = $( this ).closest( '.yith-pawc-addon-container' );
        addonContainer.remove();
    };

    // Replicate the input name text in the title. 
    replicateNameInTitle = function() {
        var addonContainer   = $( this ).closest( '.yith-pawc-addon-container' ),
            addonHeaderLabel = addonContainer.find( '.yith-pawc-header__label' );
        let value = $( this ).val();
        addonHeaderLabel.text( value );
    };

    // Show or hide options after select a field type.
    fieldTypeAction = function() {
        var addonContainer          = $( this ).closest( '.yith-pawc-addon-container' ),
            addonFieldSelect        = addonContainer.find( '.yith-pawc-field_type' ),
            fieldType               = addonFieldSelect.val(),
            inputFree               = addonContainer.find( '.yith-pawc__input-free' );
            containerOptFreeChars   = addonContainer.find( '.yith-pawc__radio-price_per_char' ),
            containerPrice          = addonContainer.find( '.yith-pawc-container__price' ),            
            containerFreeChars      = addonContainer.find( '.yith-pawc-container__free_chars' ),
            containerOptions        = addonContainer.find( '.yith-pawc-container__options' ),
            containerOptPrice       = containerOptions.find( '.yith-pawc-input_price-container' ),
            containerDefaultEnabled = addonContainer.find( '.yith-pawc-container__default_enabled' );
        switch ( fieldType ) {
            case 'text':
            case 'textarea':
                containerOptFreeChars.fadeIn( 'slow' );
                if ( inputFree.is( ':checked' ) ) {
                    containerPrice.hide();
                    containerFreeChars.hide();
                } else {
                    containerPrice.fadeIn( 'slow' );
                    containerFreeChars.fadeIn( 'slow' );
                }                
                containerOptions.hide();
                containerDefaultEnabled.hide();
                break;
            case 'select':
            case 'radio':
                containerOptFreeChars.hide();
                if ( inputFree.is( ':checked' ) ) {
                    containerPrice.hide();
                    containerFreeChars.hide();
                    containerOptPrice.hide();
                } else {
                    containerPrice.fadeIn( 'slow' );
                    containerFreeChars.fadeIn( 'slow' );
                    containerOptPrice.fadeIn( 'slow' );
                }
                containerOptions.fadeIn( 'slow' );
                containerDefaultEnabled.hide();
                break;
            case 'checkbox':
            case 'onoff':
                containerOptFreeChars.hide();
                if ( inputFree.is( ':checked' ) ) {
                    containerPrice.hide();
                } else {
                    containerPrice.fadeIn( 'slow' );
                }
                containerFreeChars.hide();
                containerOptions.hide();
                containerDefaultEnabled.fadeIn( 'slow' );
                break;
            default:
                break;        
        }   
    };

    // Show or hide options after select (or not) a free price.
    freePriceAction = function() {
        var addonContainer          = $( this ).closest( '.yith-pawc-addon-container' ),
            addonFieldSelect        = addonContainer.find( '.yith-pawc-field_type' ),
            fieldType               = addonFieldSelect.val(),
            inputFree               = addonContainer.find( '.yith-pawc__input-free' ),
            containerPrice          = addonContainer.find( '.yith-pawc-container__price' ),            
            containerFreeChars      = addonContainer.find( '.yith-pawc-container__free_chars' ),
            containerOptions        = addonContainer.find( '.yith-pawc-container__options' ),
            containerOptPrice       = containerOptions.find( '.yith-pawc-input_price-container' );            
        switch ( fieldType ) {
            case 'text':
            case 'textarea':
                if ( inputFree.is( ':checked' ) ) {
                    containerPrice.hide();
                    containerFreeChars.hide();
                } else {
                    containerPrice.fadeIn( 'slow' );
                    containerFreeChars.fadeIn( 'slow' );
                }                
                break;
            case 'select':
            case 'radio':
                if ( inputFree.is( ':checked' ) ) {
                    containerPrice.hide();
                    containerFreeChars.hide();
                    containerOptPrice.hide();
                } else {
                    containerPrice.fadeIn( 'slow' );
                    containerFreeChars.fadeIn( 'slow' );
                    containerOptPrice.fadeIn( 'slow' );
                }
                break;
            case 'checkbox':
            case 'onoff':
                if ( inputFree.is( ':checked' ) ) {
                    containerPrice.hide();
                } else {
                    containerPrice.fadeIn( 'slow' );
                }
                break;
            default:
                break;        
        }          
    };

    // Add new option to addon.
    addNewOption = function() {
        var addonContainer = $( this ).closest( '.yith-pawc-addon-container' ),
			containerOptions = addonContainer.find( '.yith-only-options' ),
			containerOption = containerOptions.find( '.yith-pawc-option-container:last-child' );
		
		containerOption.clone().appendTo( containerOptions );
        newOptionTrashSymbol = containerOptions.find( '.yith-pawc-option-container:last-child .yith-pawc-delete-option-button' );
        newOptionTrashSymbol.show();
		newOption = containerOptions.find( '.yith-pawc-option-container:last-child .yith-pawc-options' );		
        newOption.attr( 'value', '' );
        newOption.val('');
		addonContainer.find( '.yith-pawc__radio' ).trigger( 'change' );
        addonContainer.find( '.yith-pawc-field_type' ).trigger( 'change' );
    };

    // Delete addon option.
	deleteOption = function() {
		var containerOption = $( this ).closest( '.yith-pawc-option-container' );
		containerOption.remove();
	};

    // Sortable.
    $( "#pawc-sortable" ).sortable( {
        handle:'.yith-pawc-addon-head', item:'.yith-pawc-addon-container',
        update: function( event, ui ) {
            index = 0;
            $( '.yith-pawc-addon-container' ).each( function() {
                var indexInput = $( this ).find( '.yith-pawc-addon-index' );
                indexInput.attr( 'value', index );
                index++;
            });
        }
    });

    variationLoaded = function() {
        $( '.woocommerce_variable_attributes .yith-pawc-addons .yith-pawc-addon-container' ).each( function() {
            var addons = $( this ).closest( '.yith-pawc-addons' );
            addons.sortable( {
                handle: '.yith-pawc-addon-head', item: '.yith-pawc-addon-container',
                update: function( event, ui ) {
                    index = 0;
                    $( '.yith-pawc-addon-container' ).each( function() {
                        var indexInput = $( this ).find( '.yith-pawc-addon-index' );
                        indexInput.attr( 'value', index );
                        index++;
                    });
                }
            })
            $( this ).html( $( this ).html().replaceAll( '{{INDEX}}', index++ ) );
            initAddon( $( this ) );
        })
    }
    
    // Create New Add-On.
    $( document ).on( 'click', '.yith-pawc-add-new-addon', addNewAddon );

    // Arrow Add-On (slide).
    $( document ).on( 'click', '.yith-pawc-header__arrow', arrowAddon );

    // Remove Add-On (button).
    $( document ).on( 'click', '.yith-pawc-remove-addon', removeAddon );

    // Replicate the input name text in the title. 
    $( document ).on( 'keyup', '.yith-pawc-name', replicateNameInTitle );

    // Show or hide options after select a field type.
    $( document ).on( 'change', '.yith-pawc-field_type', fieldTypeAction );

    // Show or hide options after select (or not) a free price.
    $( document ).on( 'change', '.yith-pawc__radio', freePriceAction );

    // Add new option to addon.
    $( document ).on( 'click', '.yith-pawc-add-option-button', addNewOption );

    // Delete addon option.
	$( document ).on( 'click', '.yith-pawc-delete-option-button', deleteOption );

    $( document ).on( 'woocommerce_variations_loaded', variationLoaded );

    // Initialize all the addons product
    $( '.yith-pawc-addons .yith-pawc-addon-container' ).each( function() {
        $( this ).html( $( this ).html().replaceAll( '{{INDEX}}', index ) );
        index++;
        initAddon( $( this ) );
    })
});   