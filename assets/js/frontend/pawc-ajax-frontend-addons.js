jQuery(document).ready( function(){
   
    // Si vota positivo, +1
    /*jQuery('#data-vote-up').on('click', function(){*/  
        //La llamada AJAX
        /*jQuery.ajax({
            type : 'post',
            url : wp_ajax_tets_vars.ajax_url,
            data : {
                //id: jQuery(this).attr('data-id'),
                action: 'testimonials_notify_button_click', 
                message: "vote-up"
            },
            error: function(response){
                console.log(response)
                jQuery('#txtVoteCounter').text('vote-up error')
            },
            success: function(response) {
                // Actualiza el mensaje con la respuesta
                console.log(response);
                if (! response['user_cant_vote'] ) {
                    jQuery('#txtVoteCounter').text('Suma de votos: ' + response['vote_number']);
                } else {
                    jQuery('#txtVote').text('No puede votar dos veces!'); 
                }
            }
        })*/
    /*});*/
    variationChange = function( event, variation ) {
        var post_data = {
            variation_id = variation.variation_id,
            action: 'yith_pawc_ajax_addons_variations',
            start_id: $( 'yith-pawc-addon-field' ).length + 1,
            },
            productPrice = $( '.yith-pawc-product-price__value .yith-wcpa-price' );
        yithWcpa.productPrice = variation.display_price;
        productPrice.html( priceFormat( yithWcpa.productPrice ) );
        var addons = $( '.yith-wcpa-addons' );
        addons.block( opts: {
            message: '',
            overlayCSS: {backgroundColor: '#FFFFFF', opacity: 0.8, cursor: 'wait' },
        });
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: post_data,
            url: yithWcpa.ajaxurl,
            success: function( response ) {
                var variationAddons = $( 'yith-pawc-addons-variations' );
                variationAddons.html( response['addons'] );
                initAddon( variationAddons );
            },
            complete: function() {
                calculateAddOnsPrice();
                addons.unblock();
            },
        });
    };

    $( document ).on( 'found_variation', variationChange );

});