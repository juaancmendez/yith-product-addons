jQuery( document ).ready( function( $ ) {
    
    var totalPriceAddons = 0;
    calculateAddonsPrice = function() {
        totalPriceAddons = 0;
        $( '.yith-pawc-addons-frontend .yith-pawc-addon-field' ).each( function () {
            if ( 'free' !== ( $( this ).data( 'price-setting' ) ) ) {

                var inputContainer = $( this ).find( '.yith-pawc-addon-type' );
                if ( inputContainer.is( 'input[type=text]' ) || inputContainer.is( 'textarea' ) ) {

                    totalPriceAddons += calculateTextPrice( inputContainer );

                } else if ( inputContainer.is( 'select' ) ) {

                    optionSelected = inputContainer.find( '.yith-pawc-addon-option:selected' );   
                    if ( optionSelected.length > 0 ) {
                        totalPriceAddons += optionSelected.data( 'price' );
                    }   

                } else if ( inputContainer.is( 'input[type=checkbox]:checked' ) ) {

                    totalPriceAddons += inputContainer.data( 'price' );

                } else if ( inputContainer.is( 'input[type=radio]' ) ) {

                    radiosContainer = inputContainer.closest( '.yith-pawc-addon_input' );
                    radioSelected = radiosContainer.find( '.yith-pawc-addon-radio:checked' );
                    if ( radioSelected.length > 0 ) {
                        totalPriceAddons += radioSelected.data( 'price' );
                    } 
                    
                }

            }            
        })
        showPrices( totalPriceAddons );
    }

    // Calculates the price of addons of type text and textarea.
    calculateTextPrice = function( input ) {
        input_length   = input.val().length;
        containerAddon = input.closest( '.yith-pawc-addon-field' );
        price          = containerAddon.data( 'price' );
        price_setting  = containerAddon.data( 'price-setting' );
        free_chars     = containerAddon.data( 'free-chars' );
        textPrice      = 0;

        switch ( price_setting ) {
            case 'fixed_price':
                if ( input_length > free_chars ) {
                    textPrice = price;
                } else {
                    textPrice = 0;
                }
                break;
            case 'price_per_char':
                if ( input_length > free_chars ) {
                    chars_dif = input_length - free_chars;
                    textPrice = chars_dif * price;
                } else {
                    textPrice = 0;
                }
                break;
            default:
                break;        
        }
        return textPrice;
    };

    variationChange = function( event, variation ) {
        var post_data = {
            variation_id : variation.variation_id,
            action: 'yith_pawc_ajax_addons_variations',
            start_id: $( '.yith-pawc-addon-field' ).length + 1,
            };
        productPrice = $( '.yith-pawc-product-price .price-number' );
        wp_ajax_addons_vars.product_price = variation.display_price;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: post_data,
            url: wp_ajax_addons_vars.ajax_url,
            success: function( response ) {
                var variationAddons = $( '.yith-pawc-addons-variations' );
                variationAddons.html( response['addons'] );
            },
            complete: function() {
                calculateAddonsPrice();
            },
        });
    };

    // Show prices in section 'Price totals'.
    showPrices = function( totalPriceAddons ) {
        symbol = $( '.yith-pawc-prices' ).data( 'symbol' );
        $( '.yith-pawc-addons-price .price' ).text( symbol + totalPriceAddons );
        price_product = $( '.yith-pawc-product-price' ).data( 'price' );
        $( '.yith-pawc-total-price .price' ).text( symbol + (price_product + totalPriceAddons) );   
    }
     
    // Calculates the price of the addons each time the user enters a value in the text field.
    $( document ).on( 'keyup', '.yith-pawc-addon-text', calculateAddonsPrice );

    // Calculates the price of the addons each time the user enters a value in the textarea field.
    $( document ).on( 'keyup', '.yith-pawc-addon-textarea', calculateAddonsPrice );

    // Calculates the price of the addons each time the user select an option in the select field.
    $( document ).on( 'change', '.yith-pawc-addon-select', calculateAddonsPrice );

    // Calculates the price of the addons each time the user checked option in the checkbox/onoff field.
    $( document ).on( 'change', '.yith-pawc-addon-checkbox', calculateAddonsPrice );

    // Calculates the price of the addons each time the user checked option in the checkbox/onoff field.
    $( document ).on( 'change', '.yith-pawc-addon-radio', calculateAddonsPrice );    

    $( document ).on( 'found_variation', variationChange );

    // Show prices for first time.
    calculateAddonsPrice();
     
});
