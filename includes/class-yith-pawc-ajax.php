<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PAWC_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PAWC_Ajax' ) ) {
	/**
	 * YITH_PAWC_Ajax
	 */
	class YITH_PAWC_Ajax {
		/**
		 * Main Instance
		 *
		 * @var YITH_PAWC_Ajax
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PAWC_Ajax Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PAWC_Ajax constructor.
		 */
		private function __construct() {

			add_action( 'wp_ajax_yith_pawc_ajax_addons_variations', array( $this, 'yith_pawc_ajax_addons_variations' ) );
			add_action( 'wp_ajax_nopriv_yith_pawc_ajax_addons_variations', array( $this, 'yith_pawc_ajax_addons_variations' ) );

		}
		/**
		 * Función que procesa la llamada AJAX
		 *
		 * @return void
		 */
		public function yith_pawc_ajax_addons_variations() {

			//error_log( 'entre a la funcion' );
			ob_start();
			if ( isset( $_POST['variation_id'] ) ) {
				$variation_id = intval( $_POST['variation_id'] );
				$product      = wc_get_product( $variation_id );
				$addons       = $product->get_meta( 'yith-pawc-addons' );

				foreach ( $addons as $addon ) {
					$addon['product_id'] = $variation_id;
					$addon['index']      += $_POST['start_id'];
					if ( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] ) {
						$args = array( 'addon' => $addon );
						yith_pawc_get_template( '/addon-fields' . '/' . $addon['field_type'] . '.php', $args );
					}
				}
			}
			wp_send_json( array( 'addons' => ob_get_clean() ) );

		}
	}
}
