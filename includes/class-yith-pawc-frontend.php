<?php
/**
 * This file belongs to the YITH PNFW Purchase Note for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PAWC_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PAWC_Frontend' ) ) {
	/**
	 * YITH_PAWC_Frontend
	 */
	class YITH_PAWC_Frontend {
		/**
		 * Main Instance
		 *
		 * @var YITH_PAWC_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PAWC_Frontend Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PAWC_Frontend constructor.
		 */
		private function __construct() {

			add_action( 'wp_enqueue_scripts', array( $this, 'yith_pawc_enqueue_scripts' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_pawc_show_product_addons' ), 9 );
			add_filter( 'woocommerce_add_cart_item', array( $this, 'yith_pawc_add_on_cart_addons_data' ), 10, 2 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_pawc_add_on_display_cart' ), 10, 2 );
			//add_action( 'woocommerce_add_order_item_meta', array( $this, 'yith_pawc_add_on_order_item_meta' ), 10, 2 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'yith_pawc_custom_cart_item_price' ), 20, 1 );
			//add_filter( 'woocommerce_order_item_product', array( $this, 'yith_pawc_add_on_display_order' ), 10, 2 );

		}
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function yith_pawc_enqueue_scripts() {

			wp_register_style( 'yith-pawc-frontend-addons-css', YITH_PAWC_DIR_ASSETS_CSS_URL . '/frontend/pawc-frontend-addons.css', array(), YITH_PAWC_VERSION );
			wp_register_script( 'yith-pawc-frontend-addons-js', YITH_PAWC_DIR_ASSETS_JS_URL . '/frontend/pawc-frontend-addons.js', array( 'jquery' ), YITH_PAWC_VERSION ); //phpcs:ignore
			if ( is_product() || is_shop() ) {
				wp_enqueue_style( 'yith-pawc-frontend-addons-css' );
				wp_enqueue_script( 'yith-pawc-frontend-addons-js' );
				wp_localize_script(
					'yith-pawc-frontend-addons-js',
					'wp_ajax_addons_vars',
					array(
						'ajax_url'      => admin_url( 'admin-ajax.php' ),
						'product_price' => wc_get_product()->get_price(),
					),
				);
			}

		}
		/**
		 * Make and show the html Note
		 *
		 * @return void
		 */
		public function yith_pawc_show_product_addons() {
			global $post;
			$product = wc_get_product( $post->ID );
			$addons  = $product->get_meta( 'yith-pawc-addons' );

			if ( null !== $addons ) {
				$args = array( 'addons' => $addons );
				yith_pawc_get_template( '/addons-container.php', $args );
			}
		}
		/**
		 * Yith_pawc_add_on_cart_addons_data
		 *
		 * @param  mixed $cart_item
		 * @param  mixed $product_id
		 * @return void
		 */
		public function yith_pawc_add_on_cart_addons_data( $cart_item_data, $cart_item_key ) {
			$product = $cart_item_data['data'];
			if ( $product->is_type( 'variation' ) ) {
				//error_log('pase por variaton');
				$addons          = $product->get_meta( 'yith-pawc-addons' );
				$addons          = ! ! $addons && is_array( $addons ) ? $addons : array();
				$general_product = wc_get_product( $product->get_parent_id() );
				//error_log(print_r('hola' . $general_product,true));
				$general_addons  = $general_product->get_meta( 'yith-pawc-addons' );
				//error_log(print_r('hola' . $general_addons,true));
				$general_addons  = ! ! $general_addons && is_array( $general_addons ) ? $general_addons : array();
				$start_index     = count( $general_addons ) + 1;
				foreach ( $addons as &$addon ) {
					$addon['index'] += $start_index;
				}
				$addons = array_merge( $addons, $general_addons );
				//error_log(print_r('hola' . $addons,true));
			} else {
				error_log('no pase por variation');
				$addons = $product->get_meta( 'yith-pawc-addons' );
			}
			if ( isset( $_POST['yith-pawc-field'] ) && isset( $addons ) ) {
				/*$product        = wc_get_product( $product_id );
				$addons         = $product->get_meta( 'yith-pawc-addons' );*/
				//error_log(var_dump($addons));
				$addons_to_cart = $_POST['yith-pawc-field'];
				//error_log(print_r($_POST['yith-pawc-field'],true));
				if ( null !== $addons && ( null !== $addons_to_cart && '' !== $addons_to_cart && is_array( $addons_to_cart ) ) ) {
					$total_price = 0;
					foreach ( $addons as $addon ) :
						if ( ( 'yes' === $addon['enabled'] ) && ( null !== $addons_to_cart[ $addon['index'] ] ) ) :
							$content       = $addons_to_cart[ $addon['index'] ];
							$price_setting = $addon['price_setting'];
							if ( 'select' === $addon['field_type'] || 'radio' === $addon['field_type'] ) {
								foreach ( $addon['options']['name'] as $key => $value ) :
									if ( $content === $value ) {
										$price = isset( $addon['options']['price'][ $key ] ) && ! empty( $addon['options']['price'][ $key ] ) ? $addon['options']['price'][ $key ] : 0;
									}
								endforeach;
							} else {
								switch ( $price_setting ) :
									case 'free':
										$price = 0;
										break;
									case 'fixed_price':
										$price = ( strlen( $content ) - $addon['free_chars'] ) > 0 ? $addon['price'] : 0;
										break;
									case 'price_per_char':
										$chars_dif = strlen( $content ) - $addon['free_chars'];
										$price     = ( $chars_dif > 0 ) ? ( $chars_dif * $addon['price'] ) : 0;
										break;
									default:
										break;
								endswitch;
							}
							$info = array(
								'name'    => $addon['name'],
								'content' => $content,
								'price'   => $price,
							);
							$total_info[] = $info;
							$total_price += $price;
						endif;
					endforeach;
				}
				$cart_item_data['final_info']  = $total_info;
				$cart_item_data['base_price']  = $product->get_price();
				$cart_item_data['final_price'] = $product->get_price() + $total_price;
			}
			return $cart_item_data;
		}
		/**
		 * Pnfw_product_add_on_display_cart
		 *
		 * @param  mixed $data
		 * @param  mixed $cart_item
		 * @return void
		 */
		public function yith_pawc_add_on_display_cart( $data, $cart_item ) {
			if ( isset( $cart_item['final_info'] ) ) {
				$data[] = array(
					'name'  => 'Base price',
					'value' => wp_strip_all_tags( wc_price( $cart_item['base_price'] ) ),
				);
				foreach ( $cart_item['final_info'] as $item_info ) {
					$data[] = array(
						'name'  => $item_info['name'] . ' (+' . wp_strip_all_tags( wc_price( $item_info['price'] ) ) . ')',
						'value' => $item_info['content'],
					);
				}
			}
			return $data;
		}
		/**
		 * Add Add-on on CheckOut and Order Page
		 *
		 * @param  mixed $item
		 * @param  mixed $cart_item_key
		 * @param  mixed $values
		 * @param  mixed $order
		 * @return void
		 */
		public function yith_pawc_add_order_item_meta( $item_id, $values ) {
			if ( isset( $values['final_info'] ) && ! empty( $values['final_info'] ) ) {

				foreach ( $values['final_info'] as $item_info ) {

					$values = 'hola';

					wc_add_order_item_meta(
						$item_id,
						'addons',
						$item_info['final_info'],
						true,
					);

				}
			}
		}
		/**
		 * Pnfw_product_add_on_order_item_meta
		 *
		 * @param  mixed $item_id
		 * @param  mixed $values
		 * @return void
		 */
		/*public function pnfw_product_add_on_order_item_meta( $item_id, $values ) {
			if ( ! empty( $values['_yith_pnfw_content_note'] ) ) {
				wc_add_order_item_meta( $item_id, $values['title'], $values['_yith_pnfw_content_note'], true );
			}
		}*/
		/**
		 * Pnfw_product_add_on_display_order
		 *
		 * @param  mixed $cart_item
		 * @param  mixed $order_item
		 * @return void
		 */
		public function yith_pawc_add_on_display_order( $cart_item, $order_item ) {
			if ( isset( $order_item['addons'] ) ) {
				$cart_item['final_info'] = $order_item['final_info'];
			}
			return $cart_item;
		}
		/**
		 * Pnfw_custom_cart_item_price
		 *
		 * @param  mixed $cart
		 * @return void
		 */
		public function yith_pawc_custom_cart_item_price( $cart ) {
			if ( is_admin() && ! defined( 'DOING_AJAX' ) )
				return;

			if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
				return;

			foreach ( $cart->get_cart() as $cart_item ) {
				if ( null !== $cart_item['final_price'] ) {
					$cart_item['data']->set_price( $cart_item['final_price'] );
				}
			}
		}
	}
}
