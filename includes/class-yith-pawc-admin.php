<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

if ( ! defined( 'YITH_PAWC_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PAWC_Admin' ) ) {
	/**
	 * YITH_PAWC_Admin
	 */
	class YITH_PAWC_Admin {
		/**
		 * Main Instance
		 *
		 * @var YITH_PAWC_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PAWC_Admin Main instance
		 * @author Juan Coronel Mendez
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PAWC_Admin constructor.
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'pawc_create_addons_tab' ) );
			add_filter( 'woocommerce_product_data_panels', array( $this, 'product_addons_panel' ) );
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'save_addons' ) );
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'yith_pawc_add_addon_in_variations' ), 10, 3 );
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'yith_pawc_save_addon_in_variations' ), 10, 2 );
			add_action( 'admin_enqueue_scripts', array( $this, 'pawc_enqueue_scripts' ) );
		}
		/**
		 * Enqueue_scripts.
		 *
		 * @return void
		 */
		public function pawc_enqueue_scripts() {
			wp_register_style( 'yith-pawc-admin-addons-options-css', YITH_PAWC_DIR_ASSETS_CSS_URL . '/admin/pawc-admin-addons-options.css', array(), YITH_PAWC_VERSION );
			wp_register_script( 'yith-pawc-admin-addons-options-js', YITH_PAWC_DIR_ASSETS_JS_URL . '/admin/pawc-admin-addons-options.js', array( 'jquery' ), YITH_PAWC_VERSION, true );
			if ( is_admin() ) {
				wp_enqueue_style( 'yith-pawc-admin-addons-options-css' );
				wp_enqueue_script( 'yith-pawc-admin-addons-options-js' );
			}
		}

		/**
		 * Add the new tab to the $tabs array
		 *
		 * @param array $tabs .
		 * @since   1.0.0
		 */
		public function pawc_create_addons_tab( $tabs ) {
			$tabs['product_addons'] = array(
				'label'    => __( 'Add-ons', 'yith-product-addons' ),
				'target'   => 'product_addons_panel',
				'class'    => array( 'addons_tab', 'show_if_simple', 'show_if_variable' ),
				'priority' => 30,
			);
			return $tabs;
		}

		/**
		 * Print addons panel
		 *
		 * @return void
		 */
		public function product_addons_panel() {
			global $post;
			$product = wc_get_product( $post->ID );
			$addons  = $product->get_meta( 'yith-pawc-addons' );
			$addons  = is_array( $addons ) ? $addons : array();
			echo '<div id="product_addons_panel" class="panel">';
			yith_pawc_get_view( '/addons-container.php', compact( 'addons' ) );
			echo '</div>';
		}

		/**
		 * Save the custom fields using CRUD method
		 * @param $post_id
		 * @since 1.0.0
		 */
		public function save_addons( $product ) {
			if ( ! isset( $_POST['yith-pawc-addon'][0] ) ) {
				return;
			}

			foreach ( $_POST['yith-pawc-addon'][0] as $key => $addon ) {
				$addons[ $key ] = $addon;
			}

			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}

			uasort( $addons, array( $this, 'sort_by_index' ) );
			$product->update_meta_data( 'yith-pawc-addons', $addons );
			$product->save();
		}
		/**
		 * yith_pawc_add_addon_in_variations
		 *
		 * @param  mixed $loop
		 * @param  mixed $variation_data
		 * @param  mixed $variation
		 * @return void
		 */
		public function yith_pawc_add_addon_in_variations( $loop, $variation_data, $variation ) {
			$product_variation = wc_get_product( $variation );
			$addons            = $product_variation->get_meta( 'yith-pawc-addons' );
			//$product_variation->delete_meta_data( 'yith-pawc-addons' );
			error_log( 'el meta es: ' . print_r( $addons, true ) );
			$addons            = is_array( $addons ) ? $addons : array();
			yith_pawc_get_view( '/addons-container.php', compact( 'addons', 'loop' ) );
		}
		/**
		 * yith_pawc_save_addon_in_variations
		 *
		 * @param  mixed $variation
		 * @param  mixed $i
		 * @return void
		 */
		public function yith_pawc_save_addon_in_variations( $variation, $i ) {
			$i++;
			//$variation = wc_get_product( $variation_id );
			if ( ! isset( $_POST['yith-pawc-addon'] ) || ! isset( $_POST['yith-pawc-addon'][ $i ] ) ) {
				return;
			}
			//error_log('post : ' . print_r($_POST['yith-pawc-addon'], true));
			
			foreach ( $_POST['yith-pawc-addon'][ $i ] as $key => $addon ) {
				$addons[ $key ] = $addon;
			}

			//error_log('addons : ' . print_r($addons, true));

			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}

			//error_log('addons sin index : ' . print_r($addons, true));

			uasort( $addons, array( $this, 'sort_by_index' ) );
			//error_log('addons uasort : ' . print_r($addons, true));
			$variation->update_meta_data( 'yith-pawc-addons', $addons );
			//$variation->save();
			//$variation->delete_meta_data( 'yith-pawc-addons' . $variation->get_id(), $addons );
		}
		/**
		 * Sort_by_index
		 *
		 * @param  mixed $addon_prev
		 * @param  mixed $addon_next
		 * @return void
		 */
		public function sort_by_index( $addon_prev, $addon_next ) {
			return intval( $addon_prev['index'] ) > intval( $addon_next['index'] );
		}
	}
}
