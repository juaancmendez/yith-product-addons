<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH Product Addons for WooCommerce
 */

use Twig\Error\Error;

/* Addon backend template*/

$loop = isset( $loop ) ? $loop : 0;

$field_types = array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' );

$price_settings = array( 'free', 'fixed_price', 'price_per_char' )
?>
<div class="yith-pawc-addon-container" id="yith-pawc-addon-container">
	<div class="yith-pawc-addon-head">
		<button type="button" class="yith-pawc-btn-arrow"><span class="dashicons dashicons-arrow-up-alt2 yith-pawc-header__arrow"></span></button>
		<span class="yith-pawc-header__label"><?php echo esc_html( isset( $addon['name'] ) ? $addon['name'] : '' ); ?></span>
		<span class="yith-pawc_toggle-addon yith-pawc_toggle-field-input">
			<label for="yith-pawc-addon-enabled-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc_toggle-field-input__toggle switch">
				<input type="checkbox" class="yith-pawc_toggle-field-input__input yith-pawc-addon-enabled" name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][enabled]"
				value="yes"
				id="yith-pawc-addon-enabled-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" <?php checked( isset( $addon['enabled'] ) && 'yes' === $addon['enabled'] ); ?>>			
				<span class="slider round"></span>
			</label>       
		</span>
	</div>

	<div class="yith-pawc-addon">
		<div class="yith-pawc-addon-body">

			<!-- Index field (text) -->
			<input type="hidden" class="yith-pawc-addon-index" name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][index]"
				value="<?php echo intval( isset( $addon['index'] ) ? $addon['index'] : - 1 ); ?>">

			<!-- Name field (text) -->
			<div class="yith-pawc-field__container">
				<label for="yith-pawc-name-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label"><?php esc_html_e( 'Name', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container">
					<input type="text" id="yith-pawc-name-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__input yith-pawc-name"
						name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][name]"
						value="<?php echo esc_html( isset( $addon['name'] ) ? $addon['name'] : '' ); ?>">
					<div class="yith-pawc-field-description"><?php esc_html_e( 'The name of the Add-on', 'yith-product-addons' ); ?></div>       
				</div>       
			</div>

			<!-- Description field (textarea) -->
			<div class="yith-pawc-field__container">
				<label for="yith-pawc-description-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label"><?php esc_html_e( 'Description', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container">
					<textarea id="yith-pawc-description-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__input yith-pawc-description"
						name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][description]"
						value="<?php echo esc_html( isset( $addon['description'] ) ? $addon['description'] : '' ); ?>"><?php echo esc_html( isset( $addon['description'] ) ? $addon['description'] : '' ); ?></textarea>
					<div class="yith-pawc-field-description"><?php esc_html_e( 'Set here the description that will be shown above the Add-on field', 'yith-product-addons' ); ?></div>       
				</div>       
			</div>  

			<!-- Field Type field (select) -->
			<div class="yith-pawc-field__container">
				<label for="yith-pawc-field_type-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label"><?php esc_html_e( 'Field Type', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container">
					<select name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][field_type]" id="yith-pawc-field_type-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field_type">
						<option disabled selected><?php esc_html_e( 'Select', 'yith-product-addons' ); ?>:</option>
						<?php foreach ( $field_types as $f_type ) { ?>
							<option value="<?php echo esc_attr( $f_type ); ?>" <?php selected( isset( $addon['field_type'] ) && $f_type === $addon['field_type'] ); ?>><?php echo esc_html( $f_type ); ?></option>
							<?php
						}
						?>
					</select>
					<div class="yith-pawc-field-field_type"><?php esc_html_e( 'Choose the type of Add-on field', 'yith-product-addons' ); ?></div>       
				</div>       
			</div>

			<!-- Price Setting field (radio) -->
			<div class="yith-pawc-field__container">
				<label for="yith-pawc-price_setting-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label"><?php esc_html_e( 'Price Setting', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container">
					<ul class="yith-pawc-radios">
						<?php foreach ( $price_settings as $price_type ) : ?>
						<li class="yith-pawc__radio yith-pawc__radio-<?php echo esc_attr( $price_type ); ?>">
							<label>
								<input class="yith-pawc__input-<?php echo esc_attr( $price_type ); ?>" type="radio" name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price_setting]" id="yith-pawc-input__<?php echo esc_attr( $price_type ); ?>-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" 
								value="<?php echo esc_attr( $price_type ); ?>" <?php checked( isset( $addon['price_setting'] ) && $price_type === $addon['price_setting'] ); ?>><?php echo esc_html( ucwords( strtr( $price_type, '_', ' ' ) ) ); ?>
							</label>
						</li>
						<?php endforeach; ?>
					</ul>
					<div class="yith-pawc-field-price_setting"><?php esc_html_e( 'Choose how the Add-on will influence the product price', 'yith-product-addons' ); ?></div>       
				</div>       
			</div>

			<!-- Price field (number) -->
			<div class="yith-pawc-field__container yith-pawc-container__price">
				<label for="yith-pawc-price-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label"><?php esc_html_e( 'Price', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container">
					<input type="number" id="yith-pawc-price-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__input yith-pawc-price"
						name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price]"
						value="<?php echo esc_html( isset( $addon['price'] ) ? $addon['price'] : '' ); ?>">
					<div class="yith-pawc-field-price"><?php esc_html_e( 'The price of the Add-on', 'yith-product-addons' ); ?></div>       
				</div>       
			</div>

			<!-- Free Characters field (number) -->
			<div class="yith-pawc-field__container yith-pawc-container__free_chars">
				<label for="yith-pawc-free_chars-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label"><?php esc_html_e( 'Free Characters', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container">
					<input type="number" id="yith-pawc-free_chars-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__input yith-pawc-free_chars"
						name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][free_chars]"
						value="<?php echo esc_html( isset( $addon['free_chars'] ) ? $addon['free_chars'] : '' ); ?>">
					<div class="yith-pawc-field-free_chars"><?php esc_html_e( 'Choose the number of free characters', 'yith-product-addons' ); ?></div>       
				</div>       
			</div>

			<!-- Options field (text | number) -->
			<div class="yith-pawc-field__container yith-pawc-container__options">
				<label for="yith-pawc-options-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label"><?php esc_html_e( 'Options', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container yith-pawc-options-container">
					<div class="yith-only-options">
					<?php
					if ( isset( $addon['options'] ) ) :
						foreach ( $addon['options']['name'] as $key => $addon_opt ) :
							?>
							<div class="yith-pawc-option-container">
								<div class="yith-pawc-input_name-container">
									<label><?php esc_html_e( 'Name', 'yith-product-addons' ); ?><br>
									<input type="text" id="yith-pawc-options-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__input yith-pawc-options"
										name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][options][name][]"
										value="<?php echo esc_html( isset( $addon['options']['name'][ $key ] ) ? $addon['options']['name'][ $key ] : '' ); ?>">
									</label>
								</div>
								<div class="yith-pawc-input_price-container">
									<label for=""><?php esc_html_e( 'Price', 'yith-product-addons' ); ?><br>
									<input type="number" id="yith-pawc-options-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__input yith-pawc-options"
										name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][options][price][]"
										value="<?php echo esc_html( isset( $addon['options']['price'][ $key ] ) ? $addon['options']['price'][ $key ] : '' ); ?>">
									</label>
								</div>
								<span class="dashicons dashicons-trash yith-pawc-delete-option-button"></span>
							</div>
							<?php
						endforeach;
					endif;
					?>
				</div>
				<div class="yith-pawc-add-new-option">
					<span class="yith-pawc-add-option-button">+ <?php esc_html_e( 'Add New Option', 'yith-product-addons' ); ?></span>
				</div>  
				<div class="yith-pawc-field-options"><?php esc_html_e( 'Set the options for Add-on', 'yith-product-addons' ); ?></div> 
				</div>
			</div>

			<!-- Default Enabled field (checkbox) -->
			<div class="yith-pawc-field__container yith-pawc-container__default_enabled">
				<label for="yith-pawc-default_enabled-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" class="yith-pawc-field__label switch"><?php esc_html_e( 'Default Enabled', 'yith-product-addons' ); ?></label>
				<div class="yith-pawc-input_container">
					<input type="checkbox" class="yith-pawc_toggle-field-input__input yith-pawc-default_enabled yith-proteo-standard-checkbox" name="yith-pawc-addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][default_enabled]"
					value="yes"
					id="yith-pawc-default_enabled-<?php echo esc_attr( $loop ); ?>-{{INDEX}}" <?php checked( isset( $addon['default_enabled'] ) && 'yes' === $addon['default_enabled'] ); ?>>
					<div class="yith-pawc-field-default_enabled"><?php esc_html_e( 'Enable if the field is enabled by default', 'yith-product-addons' ); ?></div>
					<span class="slider_de round_de"></span> 
				</div>
			</div>
			<div class="yith-pawc-remove-addon__container">
				<span class="yith-pawc-remove-addon"><?php esc_html_e( 'REMOVE ADD-ON', 'yith-product-addons' ); ?></span>
			</div> 
		</div>
	</div>
</div>
