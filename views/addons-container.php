<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH Product Addons for WooCommerce
 */

// Backend Addons container.

$default_values = array(
	'enabled'         => 'yes',
	'index'           => 0,
	'name'            => __( 'Untitle', 'yith-product-addons' ),
	'description'     => '',
	'field_type'      => 'text',
	'price_setting'   => 'free',
	'price'           => 0,
	'free_chars'      => 0,
	'options'         => array(
		'name'  => array(
			0 => '',
		),
		'price' => array(
			0 => '',
		),
	),
	'default_enabled' => 'no',
	'loop'            => 0,
);

$default_args = array( 'addon' => $default_values );

if ( isset( $loop ) ) {
	$default_args['loop'] = $loop + 1;
}

?>

<div class="yith-pawc-total-container">
	<span class="yith-pawc-add-new-addon"><?php esc_html_e( 'ADD NEW ADD-ON', 'yith-product-addons' ); ?></span>
	<div class="yith-pawc-template yith-pawc-hidden">
		<?php yith_pawc_get_view( '/addon.php', $default_args ); ?>
	</div>
	<div class="yith-pawc-addons" id="pawc-sortable">
		<?php
		//global $post;
		//$product = wc_get_product( $post->ID );
		//$addons  = $product->get_meta( 'yith-pawc-addons' );
		//error_log( 'el meta es: ' . print_r( $addons, true ) );
		foreach ( $addons as $addon ) {
			$args = array( 'addon' => $addon );
			if ( isset( $loop ) ) {
				$args['loop'] = $loop + 1;
			}
			yith_pawc_get_view( '/addon.php', $args );
		}
		?>
	</div>
</div>
