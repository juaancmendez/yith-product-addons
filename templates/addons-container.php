<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package YITH Product Addons for WooCommerce
 */

// Frontend Addons container.
?>

<div class="yith-pawc-addons-frontend">
	<div class="yith-pawc-addons-variations"></div>
	<?php
	global $post;
	$product = wc_get_product( $post->ID );
	//error_log( 'frontend: el meta es: ' . print_r( $addons, true ) );
	foreach ( $addons as $addon ) {
		if ( 'yes' === $addon['enabled'] ) {
			$args = array( 'addon' => $addon );
			yith_pawc_get_template( '/addon-fields' . '/' . $addon['field_type'] . '.php', $args ); //phpcs:ignore
		}
	}
	?>
	<div class="yith-pawc-prices" data-symbol="<?php echo esc_attr( get_woocommerce_currency_symbol() ); ?>">		
		<div class="yith-pawc-prices-title">
			<span><?php esc_html_e( 'Price totals', 'yith-product-addons' ); ?></span>
		</div>
		<div class="yith-pawc-product-price" data-price="<?php echo esc_attr( $product->get_price() ); ?>">
			<span>Product price:</span>
			<span class="price"><?php echo esc_html( get_woocommerce_currency_symbol() ); ?><span class="price-number"><?php echo esc_html( $product->get_price() ); ?></span></span>
		</div>
		<div class="yith-pawc-addons-price">
			<span>Additional options total:</span>
			<span class="price"></span>
		</div>
		<div class="yith-pawc-total-price">
			<span>Total:</span>
			<span class="price"></span>
		</div>
	</div>
</div>
