<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

/**Addon frontend select template ...*/

?>
<div class="yith-pawc-addon-field" <?php echo ( 'free' !== $addon['price_setting'] ? 'data-price="' . esc_attr( $addon['price'] ) . '"' : '' ); ?>
									<?php echo 'data-price-setting="' . esc_attr( $addon['price_setting'] ) . '"'; ?>>
	<div class="yith-pawc-addon_name">
		<?php echo isset( $addon['name'] ) ? esc_html( $addon['name'] ) : ''; ?>
	</div>
	<div class="yith-pawc-addon_input yith-pawc-field__checkbox" name="yith-pawc-field-<?php echo intval( $addon['index'] ); ?>" id="yith-pawc-<?php echo esc_attr( $product_id ) . '-' . esc_attr( $addon['index'] ); ?>">
		<input type="checkbox" id="yith-pawc-checkbox_<?php echo intval( $addon['index'] ); ?>" name="yith-pawc-field[<?php echo intval( $addon['index'] ); ?>]" value="<?php echo esc_attr( $addon['description'] ); ?>" 
				<?php checked( isset( $addon['default_enabled'] ) && 'yes' === $addon['default_enabled'] ); ?>
				class="yith-pawc-addon-type yith-pawc-addon-checkbox" 
				<?php echo ( isset( $addon['price_setting'] ) && 'free' !== $addon['price_setting'] ? 'data-price="' . esc_attr( $addon['price'] ) . '"' : '' ); ?>>
		<label for="yith-pawc-checkbox_<?php echo intval( $addon['index'] ); ?>"> <?php echo isset( $addon['description'] ) ? esc_html( $addon['description'] ) : ''; ?></label>   
		<?php
		$option_text = ( 'free' !== $addon['price_setting'] ? ' (+ ' . wp_strip_all_tags( wc_price( $addon['price'] ) ) . ')' : '' );
		?>
		<p class="yith-pawc-addon-price"><?php echo esc_html( $option_text ); ?></p>
	</div>
</div>
