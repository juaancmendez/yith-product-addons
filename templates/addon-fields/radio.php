<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

/**Addon frontend select template ...*/

?>
<div class="yith-pawc-addon-field" <?php echo ( 'free' !== $addon['price_setting'] ? 'data-price="' . esc_attr( $addon['price'] ) . '"' : '' ); ?>
									<?php echo 'data-price-setting="' . esc_attr( $addon['price_setting'] ) . '"'; ?>>
	<div class="yith-pawc-addon_name">
		<?php echo isset( $addon['name'] ) ? esc_html( $addon['name'] ) : ''; ?>
	</div>
	<div class="yith-pawc-addon_description">
		<?php echo isset( $addon['description'] ) ? esc_html( $addon['description'] ) : ''; ?>
	</div>
	<ul class="yith-pawc-addon_input yith-pawc-field__radio" id="yith-pawc-<?php echo esc_attr( $addon['index'] ); ?>">
		<?php foreach ( $addon['options']['name'] as $key => $value ) : ?>
		<li>
		<input type="radio" id="yith-pawc-radio<?php echo esc_attr( $key ); ?>" name="yith-pawc-field[<?php echo esc_attr( $addon['index'] ); ?>]" 
				value="<?php echo esc_attr( $addon['options']['name'][ $key ] ); ?>" 
				<?php echo ( 'free' !== $addon['price_setting'] ? 'data-price="' . esc_attr( $addon['options']['price'][ $key ] ) . '"' : '' ); ?>
				class="yith-pawc-addon-type yith-pawc-addon-radio">
			<?php
			$option_text = esc_html( $addon['options']['name'][ $key ] );
			//if ( $show_price ) {
				$option_text .= ( 'free' !== $addon['price_setting'] && ! ! $addon['options']['price'][ $key ] ? ' (+ ' . wp_strip_all_tags( wc_price( $addon['options']['price'][ $key ] ) ) . ')' : '' );
			//}
			?>
			<label for="yith-pawc-radio<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $option_text ); ?></label>
		</li>
		<?php endforeach; ?>        
	</select>
</div>
