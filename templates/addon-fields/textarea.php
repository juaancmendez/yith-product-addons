<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

/**Addon frontend select template ...*/

?>
<div class="yith-pawc-addon-field" <?php echo ( 'free' !== $addon['price_setting'] ? 'data-price="' . esc_attr( $addon['price'] ) . '"' : '' ); ?>
									<?php echo 'data-price-setting="' . esc_attr( $addon['price_setting'] ) . '"'; ?>
									<?php echo 'data-free-chars="' . esc_attr( $addon['free_chars'] ) . '"'; ?>>
	<div class="yith-pawc-addon_name">
		<?php echo isset( $addon['name'] ) ? esc_html( $addon['name'] ) : ''; ?>
	</div>
	<div class="yith-pawc-addon_description">
		<?php echo isset( $addon['description'] ) ? esc_html( $addon['description'] ) : ''; ?>
	</div>
	<div class="yith-pawc-addon_input yith-pawc-field__textarea" name="yith-pawc-field-<?php echo intval( $addon['index'] ); ?>" id="yith-pawc-<?php echo esc_html( $addon['index'] ); ?>">
		<textarea name="yith-pawc-field[<?php echo intval( $addon['index'] ); ?>]" class="yith-pawc-addon-type yith-pawc-addon-textarea" required></textarea>
		<?php
		$option_text = ( 'free' !== $addon['price_setting'] ? ' (+ ' . wp_strip_all_tags( wc_price( $addon['price'] ) ) . ')' : '' );
		?>
		<p class="yith-pawc-addon-price"><?php echo esc_html( $option_text ); ?></p>     
	</div>
</div>	
