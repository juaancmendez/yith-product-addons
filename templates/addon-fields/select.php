<?php
/**
 * This file belongs to the YITH Product Addons for WooCommerce.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package .
 */

/**Addon frontend select template ...*/

?>
<div class="yith-pawc-addon-field" <?php echo 'data-price-setting="' . esc_attr( $addon['price_setting'] ) . '"'; ?>>
	<div class="yith-pawc-addon_name">
		<?php echo isset( $addon['name'] ) ? esc_html( $addon['name'] ) : ''; ?>
	</div>
	<div class="yith-pawc-addon_description">
		<?php echo isset( $addon['description'] ) ? esc_html( $addon['description'] ) : ''; ?>
	</div>
	<div class="yith-pawc-addon_input">
		<select class="yith-pawc-addon-type yith-pawc-addon-select" name="yith-pawc-field[<?php echo intval( $addon['index'] ); ?>]" id="yith-pawc-field-<?php echo intval( $addon['index'] ); ?>" required>
			<option disabled value=""><?php esc_html_e( 'Select an option', 'yith-product-addon' ); ?></option>
			<?php foreach ( $addon['options']['name'] as $key => $value ) : ?>
			<option value="<?php echo esc_attr( $addon['options']['name'][ $key ] ); ?>" 
					<?php echo ( isset( $addon['price_setting'] ) && 'free' !== $addon['price_setting'] ? 'data-price="' . esc_attr( $addon['options']['price'][ $key ] ) . '"' : '' ); ?>
					class="yith-pawc-addon-option">
				<?php
				$option_text  = esc_html( $addon['options']['name'][ $key ] );
				$option_text .= ( 'free' !== $addon['price_setting'] && ! ! $addon['options']['price'][ $key ] ? ' (+ ' . wp_strip_all_tags( wc_price( $addon['options']['price'][ $key ] ) ) . ')' : '' );
				echo esc_html( $option_text );
				?>
			</option>
			<?php endforeach; ?>        
		</select>
	</div>
</div>	
